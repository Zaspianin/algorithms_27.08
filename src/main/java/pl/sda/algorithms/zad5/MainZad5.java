package pl.sda.algorithms.zad5;

public class MainZad5 {
    public static void main(String[] args) {
        System.out.println("fibo: 17"+fibonacci(100));
    }

    public static int fibonacci(int n) {
        //funkcja rekurencyjna-musi zwracać samą siebie
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
